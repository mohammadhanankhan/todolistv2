const createError = require('http-errors');
const { subtaskSchema } = require('../helpers/validation');
const Subtask = require('../models/Subtask');
const Task = require('../models/Task');

module.exports = {
  index: async (req, res, next) => {
    try {
      const allSubTasks = await Subtask.findAll();

      res.json({ allSubTasks });
    } catch (error) {
      next(error);
    }
  },
  show: async (req, res, next) => {
    try {
      const { id: subtaskId = null } = req.params;

      if (isNaN(subtaskId)) {
        throw createError.BadRequest();
      }

      const subTask = await Subtask.findOne({
        where: {
          id: subtaskId,
        },
      });

      if (subTask === null) {
        throw createError.NotFound();
      } else {
        res.json({ subTask });
      }
    } catch (error) {
      next(error);
    }
  },
  create: async (req, res, next) => {
    try {
      const response = await subtaskSchema.validateAsync(req.body);

      const { subtask, status, task_id } = response;

      const userId = req.payload.aud;

      // SEE IF THE TASK_ID BELONGS TO THE USER
      const check = await Task.findAll({
        where: {
          id: task_id,
          user_id: userId,
        },
      });

      // IF NOT THROW AN ERROR
      if (check.length === 0) {
        throw createError.Unauthorized();
      }

      // ELSE INSERT THE SUBTASK
      const result = await Subtask.create({
        subtask,
        status,
        task_id,
      });

      const newSubTask = result.dataValues;

      res.status(201).send({ newSubTask });
    } catch (error) {
      if (error.isJoi === true) {
        error.status = 400;
      }
      next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      const { id: subtaskId = null } = req.params;

      if (isNaN(subtaskId)) {
        throw createError.BadRequest();
      }
      const response = await subtaskSchema.validateAsync(req.body);

      const { subtask, status, task_id } = response;

      const userId = req.payload.aud;

      // SEE IF THE TASK_ID BELONGS TO THE USER
      const check = await Task.findAll({
        where: {
          id: task_id,
          user_id: userId,
        },
      });

      // IF NOT THROW AN ERROR
      if (check.length === 0) {
        throw createError.Unauthorized();
      }

      // ELSE UPDATE THE SUBTASK
      const result = await Subtask.update(
        { subtask, status },
        {
          where: {
            id: subtaskId,
            task_id,
          },
        }
      );

      if (result[0] === 0) {
        throw createError.Unauthorized();
      }

      res.sendStatus(201);
    } catch (error) {
      if (error.isJoi === true) error.status = 400;

      next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      const { id: subtaskId = null } = req.params;
      const { task_id = null } = req.body;

      if (isNaN(subtaskId) || task_id === null) {
        throw createError.BadRequest();
      }
      const userId = req.payload.aud;

      // SEE IF THE TASK_ID BELONGS TO THE USER
      const check = await Task.findAll({
        where: {
          id: task_id,
          user_id: userId,
        },
      });

      // IF NOT THROW AN ERROR
      if (check.length === 0) {
        throw createError.Unauthorized();
      }

      // ELSE DELETE
      const result = await Subtask.destroy({
        where: {
          id: subtaskId,
          task_id,
        },
      });

      if (result === 0) {
        throw createError.Unauthorized();
      }

      res.sendStatus(201);
    } catch (error) {
      next(error);
    }
  },
};
