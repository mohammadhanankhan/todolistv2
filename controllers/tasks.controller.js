const createError = require('http-errors');
const Task = require('../models/Task');
const { taskSchema } = require('../helpers/validation');

module.exports = {
  index: async (req, res, next) => {
    try {
      const allTasks = await Task.findAll();

      res.json({ allTasks });
    } catch (error) {
      next(error);
    }
  },
  show: async (req, res, next) => {
    try {
      const { id: taskId = null } = req.params;

      if (isNaN(taskId)) {
        throw createError.BadRequest();
      }

      const task = await Task.findOne({
        where: {
          id: taskId,
        },
      });
      if (task === null) {
        throw createError.NotFound();
      } else {
        res.json({ task });
      }
    } catch (error) {
      next(error);
    }
  },
  create: async (req, res, next) => {
    try {
      const response = await taskSchema.validateAsync(req.body);

      const { task, status } = response;
      const userId = req.payload.aud;

      const newTask = await Task.create({
        task,
        status,
        user_id: userId,
      });

      res.status(201).send({ newTask });
    } catch (error) {
      if (error.isJoi === true) {
        error.status = 400;
      }
      next(error);
    }
  },
  update: async (req, res, next) => {
    try {
      const { id: taskId = null } = req.params;

      if (isNaN(taskId)) {
        throw createError.BadRequest();
      }

      const response = await taskSchema.validateAsync(req.body);

      const { task, status } = response;
      const userId = req.payload.aud;

      const result = await Task.update(
        { task, status },
        {
          where: {
            id: taskId,
            user_id: userId,
          },
        }
      );
      if (result[0] === 0) {
        throw createError.Unauthorized();
      }

      res.sendStatus(201);
    } catch (error) {
      if (error.isJoi === true) {
        error.status = 400;
      }

      next(error);
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id: taskId = null } = req.params;

      if (isNaN(taskId)) {
        throw createError.BadRequest();
      }

      const userId = req.payload.aud;

      const result = await Task.destroy({
        where: {
          id: taskId,
          user_id: userId,
        },
      });

      if (result === 0) {
        throw createError.Unauthorized();
      }

      res.sendStatus(201);
    } catch (error) {
      next(error);
    }
  },
};
