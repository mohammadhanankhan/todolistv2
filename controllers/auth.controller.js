const createError = require('http-errors');
const bcrypt = require('bcrypt');
const { authSchema } = require('../helpers/validation');
const {
  signAccessToken,
  signRefreshToken,
  verifyRefreshToken,
} = require('../helpers/jwt');
const client = require('../helpers/init_redis');
const User = require('../models/User');

module.exports = {
  register: async (req, res, next) => {
    try {
      // VALIDATE INPUT / SANITIZE INPUT
      const response = await authSchema.validateAsync(req.body);

      // CHECK IN DATABASE IF USER ALREADY EXISTS
      const result = await User.findAll({
        attributes: ['email'],
        where: {
          email: response.email,
        },
      });

      // IF EXISTS THROW AN ERROR OF CONFLICT 409
      if (result.length >= 1) {
        throw createError.Conflict(`${response.email} already exists`);
      }
      // ELSE HASH PASSWORD
      const hashedPassword = await bcrypt.hash(response.password, 12);

      // INSERT A USER
      const newUser = await User.create({
        email: response.email,
        password: hashedPassword,
      });

      const userId = newUser.dataValues.id;

      const accessToken = await signAccessToken(userId);
      const refreshToken = await signRefreshToken(userId);

      res.send({ accessToken, refreshToken });
    } catch (error) {
      if (error.isJoi === true) error.status = 422;
      next(error);
    }
  },
  login: async (req, res, next) => {
    try {
      const response = await authSchema.validateAsync(req.body);

      const result = await User.findAll({
        attributes: ['id', 'email', 'password'],
        where: {
          email: response.email,
        },
      });

      if (result.length === 0) {
        throw createError.NotFound("User doesn't exist");
      }

      const credentials = result[0].dataValues;

      const storedPassword = credentials.password;

      // CHECK IF THE PASSWORD THAT USER ENTERED MATCHES
      const isMatch = await bcrypt.compare(response.password, storedPassword);

      if (!isMatch) {
        throw createError.Unauthorized('Invalid Email/Password ');
      }

      const userId = credentials.id;

      const accessToken = await signAccessToken(userId);
      const refreshToken = await signRefreshToken(userId);

      res.send({ accessToken, refreshToken });
    } catch (error) {
      if (error.isJoi === true) {
        return next(createError.BadRequest('Invalid Email/Password'));
      }
      next(error);
    }
  },
  refreshToken: async (req, res, next) => {
    try {
      const { refreshToken = null } = req.body;

      if (refreshToken === null) {
        throw createError.BadRequest();
      }

      const userId = await verifyRefreshToken(refreshToken);

      const accessToken = await signAccessToken(userId);
      const refToken = await signRefreshToken(userId);

      res.send({ accessToken: accessToken, refreshToken: refToken });
    } catch (error) {
      next(error);
    }
  },
  logout: async (req, res, next) => {
    try {
      const { refreshToken = null } = req.body;
      if (refreshToken === null) {
        throw createError.BadRequest();
      }
      const userId = await verifyRefreshToken(refreshToken);

      client.DEL(userId, (err, val) => {
        if (err) {
          console.log(err.message);

          throw createError.InternalServerError();
        } else {
          // IN THIS CASE IT MEANS EITHER THE KEY HAS BEEN DELETED OR THE KEY DOESN'T EXIST - IN ANY OF THE USER HAS BEEN LOGGED OUT
          console.log(val);

          res.sendStatus(204); // THERE'S NO RESPONSE BUT EVERYTHING WENT OK
        }
      });
    } catch (error) {
      next(error);
    }
  },
};
