const { Sequelize } = require('sequelize');

const db = require('../config/db');

const User = db.define(
  'User',
  {
    email: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
    tableName: 'users',
    createdAt: false,
    updatedAt: false,
  }
);

module.exports = User;
