const { Sequelize } = require('sequelize');

const db = require('../config/db');

const Task = db.define(
  'Task',
  {
    task: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    user_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
    tableName: 'tasks',
    createdAt: false,
    updatedAt: false,
  }
);

module.exports = Task;
