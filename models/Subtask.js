const { Sequelize } = require('sequelize');
const db = require('../config/db');

const Subtask = db.define(
  'Subtask',
  {
    subtask: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    status: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    task_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
    tableName: 'subtasks',
    createdAt: false,
    updatedAt: false,
  }
);

module.exports = Subtask;
