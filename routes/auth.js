const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/auth.controller');

// REGISTER ROUTE
router.post('/register', AuthController.register);

// LOGIN ROUTE
router.post('/login', AuthController.login);

// REFRESH TOKEN ROUTE
router.post('/refresh-token', AuthController.refreshToken);

// LOGOUT ROUTE
router.delete('/logout', AuthController.logout);

module.exports = router;
