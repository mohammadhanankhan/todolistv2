const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const client = require('./init_redis');

module.exports = {
  signAccessToken: userId => {
    return new Promise((resolve, reject) => {
      const payload = {};

      const secret = process.env.ACCESS_TOKEN_SECRET;

      const options = {
        expiresIn: '10h',
        issuer: 'todolist.com',
        audience: userId,
      };

      jwt.sign(payload, secret, options, (err, token) => {
        if (err) {
          console.log(err.message);

          return reject(createError.InternalServerError());
        } else {
          resolve(token);
        }
      });
    });
  },
  verifyAccessToken: (req, res, next) => {
    if (!req.headers['authorization']) {
      return next(createError.Unauthorized());
    }
    const authHeader = req.headers['authorization'];

    const token = authHeader.split(' ')[1];

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
      if (err) {
        // TO HIDE ERROR MESSAGES FROM CLIENT COMING FROM 'JsonWebTokenError' FOR BETTER SECURITY LIKE 'SIGNATURE WAS INVALID', CLIENT CAN TRY DIFFERENT OPTIONS
        if (err.name === 'JsonWebTokenError') {
          return next(createError.Unauthorized());
        } else {
          return next(createError.Unauthorized(err.message));
        }
      } else {
        req.payload = payload;

        next();
      }
    });
  },

  signRefreshToken: userId => {
    return new Promise((resolve, reject) => {
      const payload = {};

      const secret = process.env.REFRESH_TOKEN_SECRET;

      const options = {
        expiresIn: '1y',
        issuer: 'todolist.com',
        audience: userId,
      };

      jwt.sign(payload, secret, options, (err, token) => {
        if (err) {
          console.log(err.message);

          return reject(createError.InternalServerError());
        } else {
          // SAVE THE REFRESH TOKEN INSIDE REDIS CACHE
          // REPLY HERE IS WHAT IS COMING AS A REPLY FROM REDIS
          // userId is set to token
          // EX IS THE EXPIRATION TIME OF THE REFRESH TOKEN
          client.SET(userId, token, 'EX', 365 * 24 * 60 * 60, (err, reply) => {
            if (err) {
              console.log(err.message);

              reject(createError.InternalServerError());

              return;
            } else {
              resolve(token);
            }
          });
        }
      });
    });
  },
  verifyRefreshToken: refreshToken => {
    return new Promise((resolve, reject) => {
      jwt.verify(
        refreshToken,

        process.env.REFRESH_TOKEN_SECRET,

        (err, payload) => {
          if (err) {
            return reject(createError.Unauthorized());
          } else {
            const userId = payload.aud; // HERE WE WRITE SHORT FORM FOR AUDIENCE

            /**
             * INSTEAD OF DIRECTLY RESOLVING, WE WANT TO SEARCH REDIS DB FOR THIS     userID (key) i.e IF THE KEY EXISTS IN REDIS DB
             */

            client.GET(userId, (err, result) => {
              if (err) {
                console.log(err.message);

                reject(createError.InternalServerError());
                return;
              }
              if (refreshToken === result) {
                // refreshToken IS THE TOKEN WE WANT TO VERIFY
                // RESULT IS THE REFRESH TOKEN COMING FROM REDIS
                return resolve(userId);
              } else {
                reject(createError.Unauthorized());
              }
            });
          }
        }
      );
    });
  },
};
